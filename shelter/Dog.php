<?php 
namespace Shelter;
require_once 'shelter/AnimalInfo.php';
require_once 'shelter/Animal.php';

class Dog extends AnimalInfo implements Animal
{
    private $dogs = array();

    public function __construct($nickname,$age,$type) {
        parent::__construct($nickname,$age,$type);
        array_push($this->dogs,parent::record());
    }

    public function getAllDogs() {

        return $this->dogs;
    }
    
  
}

?>