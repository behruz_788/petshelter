<?php
namespace Shelter;

interface Animal
{

    public function record();
    public function getType();
    public function getByType();

}

?>