<?php 
namespace Shelter;

class Shelter
{
    private $type;
    private $animals = array();
    public $animal;

    public function __construct(Animal $animal) {
        $this->animal = $animal;
    }
    public function addToShelter() {
        $this->type = $this->animal->getType();        
        array_push($this->animals, $this->animal->record());

        return $this->animals;
    }

    

}


?>