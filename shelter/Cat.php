<?php  
namespace Shelter;

require_once 'shelter/AnimalInfo.php';
require_once 'shelter/Animal.php';


class Cat extends AnimalInfo implements Animal
{
    private $cats = array();

    public function __construct($nickname,$age,$type)
    {
        parent::__construct($nickname,$age,$type);
        array_push($this->cats,parent::record());
    }

    public function getAllCats() 
    {
        return $this->cats;
    }
}



?>